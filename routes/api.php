<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::middleware('jwt.auth')->get('users', function () {
    return auth('api')->user();

});

Route::post('logout/{token}', 'AuthController@logout');
Route::post('login', 'AuthController@login');
Route::post('register', 'AuthController@register');
Route::post('verifyemail', 'AuthController@verifyEmail');
Route::post('verifysms', 'AuthController@verifySms');

//Crud Videos routes
Route::post('get_videos', 'VideoController@index');
Route::get('videos/{id}', 'VideoController@show');
Route::post('videos', 'VideoController@store');
Route::put('videos', 'VideoController@update');
Route::delete('videos/{id}', 'VideoController@delete');
Route::post('search', 'VideoController@searchVideo');

// CRUD Profiles routes
Route::post('get_profiles', 'ProfileController@index');
Route::get('profiles/{id}', 'ProfileController@show');
Route::post('profiles', 'ProfileController@store');
Route::put('profiles', 'ProfileController@update');
Route::delete('profiles/{id}', 'ProfileController@delete');