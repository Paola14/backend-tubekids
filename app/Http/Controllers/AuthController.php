<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\RegisterFormRequest;
use App\User;
use JWTAuth;
use Auth;
use Tymon\JWTAuth\Exceptions\JWTException;
use DateTime;
use DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Twilio\Rest\Client;


class AuthController extends Controller
{
    public function __construct()
    {
   
    }

    /* Method for register the user and send email for sendgrid function */
    public function register(Request $request)
    {
            $user = new User;
            $user->email = $request->email;
            $user->password = bcrypt($request->password);
            $user->name = $request->name;
            $user->last_name = $request->last_name;
            $user->country = $request->country;
            $user->birth_date = $request->birth_date;
            $user->phone_number = $request->phone_number;
            $user->confirmation_code = Str::random(10);
            $user->pin_code = rand(1,10);

            $f1 = new DateTime("{$user->birth_date}");
            $f2 = new DateTime("now");
            $diferencia =  $f1->diff($f2);

            if ($diferencia->format("%y") > 18) {
                $user->save();

                $email = new \SendGrid\Mail\Mail(); 
                $email->setFrom("tubekidsprogram@gmail.com", "Verify Email Address");
                $email->setSubject("Confirma tu registro a Tubekids!");
                $email->addTo($user->email, "Verify Email Address");
                $email->addContent(
                        "text/html", "<h2>Hola $user->name, gracias por registrarte en <strong>Tubekids</strong></h2>
                        <p>Por favor confirma tu correo electrónico, con el siguiente codigo: $user->confirmation_code</p>");
                         
                $sendgrid = new \SendGrid(getenv('SENDGRID_API_KEY'));
                $response = $sendgrid->send($email);
              
                 try {
                    return response()->json(['user' => $user, 'message' => 'User acount created successfully', 'code' => 201]);
                    } catch (Exception $e) {
                    return response()->json(['code' => 400, 'message' => 'Could not create user']);
                    }      
            } else {
                return response([
                    'status' => 'error',
                    'error' => 'failed',
                    'message' => 'You must be over 18 years old ',
                    'code' =>400
                ]);
            }
        } 

        /*Method for verify email */
        public function verifyEmail(Request $request)
        {
            $user = User::where('confirmation_code', $request->confirmation_code)->first();
        
            if (!$user){
                return response()->json(['code' => 400, 'message' => 'Could not confirmated user']);
            }

            return response()->json(['message' => 'You have successfully confirmed your email', 'code' => 200]);
        }

         /* Method for auth the user and send message for twilio function */
        public function login(Request $request)
        {  

            $credentials = request(['email', 'password']);
            $user = User::where ('email',$request->email)->first();
            $phone_user = $user->phone_number;
            $number = rand(10000,1000000);
            $user1 = User::where('email',$request->email)->update([
                "pin_code" => $number,
            ]);
            $user2 = User::where ('email',$request->email)->first();
            $pin = $user2->pin_code;
            $token = JWTAuth::attempt($credentials);
            if (!$token = auth()->attempt($credentials)) {
                return response()->json(['error' => 'Unauthorized'], 401);
            } else {
                    $sid = getenv("TWILIO_SID");
                    $stoken = getenv("TWILIO_AUTH_TOKEN");
                    $twilio_number = getenv("TWILIO_NUMBER");
                    $twilio = new Client($sid, $stoken);
                    $twilio->messages->create(
                        $phone_user,
                        array(
                            'from' => $twilio_number,
                            'body' => $pin
                        ) 
                    );
                return response()->json(['code' => 200, 'token' => $token]);
            }
        }

        /*Method for verify sms */
        public function verifySMS(Request $request)
        {
            $save_token = request(['token']);
            $user = User::where('pin_code', $request->pin_code)->first();
        
            if (! $user){
                return response()->json(['code' => 400, 'message' => 'Could not verify user']);
            }

            return response()->json(['message' => 'You have successfully confirmed your log in', 'code' => 200, 'token' => $save_token]);
        }
 
        /*Logout */
         public function logout($token)
           {
              
               try {
                   JWTAuth::invalidate($token);
        
                   return response()->json([
                       'success' => true,
                       'message' => 'User logged out successfully'
                   ]);
               } catch (JWTException $exception) {
                   return response()->json([
                       'success' => false,
                       'message' => 'Sorry, the user cannot be logged out'
                   ]);
               }
           }
       
         
    }

