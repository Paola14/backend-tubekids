<?php

namespace App\Http\Controllers;

use App\Profile;
use Illuminate\Http\Request;

class ProfileController extends Controller
{

    public function validateUser($token){
        return auth()->setToken($token)->user();
    }

     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($this->validateUser($request['token'])){
         $profiles= Profile::all();
         return response()->json(['profiles' => $profiles, 'code' => 200]);
        }else{
            return response()->json([ 'message' => 'No profiles available', 'code' => 401]);
        } 
    }
    

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($this->validateUser($request['token'])){
          $profile = Profile::create([
            'name' => $request['name'],
            'username' => $request['username'],
            'pin' => $request['pin'],
            'age' => $request['age'],
            ]);
            return response()->json([ 'profile' => $profile, 'code' => 201]);
        }else{
            return response()->json([ 'message' => 'User authentication failed', 'code' => 401]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $profile = Profile::find($id);
        return response()->json(['profile' => $profile, 'code' => 200]); 
    }

  
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        if($this->validateUser($request['token'])){
            $profile = Profile::find($request['id']);

            $profile ->name = $request ->name;
            $profile ->username = $request ->username;
            $profile ->pin = $request ->pin;
            $profile ->age = $request ->age;
            $profile -> save();
            return response()->json(['profile' => $profile, 'code' => 200]);
        }else{
            return response()->json([ 'message' => 'User authentication failed', 'code' => 401]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request)
    {
        try{
            if($this->validateUser($request['token'])){
                $profile = Profile::find($request['id']);
                $profile->delete();
                return response()->json(['code' => 200]);
            }else{
                return response()->json([ 'message' => 'User authentication failed', 'code' => 401]);
            } 
        }catch (Exception $e){
            return response()->json(['message' => 'Fatal Error', 'code' => '404']);

        }
    }
}
